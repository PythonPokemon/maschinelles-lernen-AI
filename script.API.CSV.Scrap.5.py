import requests
from bs4 import BeautifulSoup
import pandas as pd
import numpy as np
from sklearn.linear_model import LinearRegression
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error

# Funktion zum Einlesen von Daten aus einer CSV-Datei
def read_data_from_csv(csv_file):
    data = pd.read_csv(csv_file)
    return data

# Funktion zum Web-Scrapen von Daten
def scrape_data_from_website(url):
    # Führen Sie Web Scraping durch und extrahieren Sie Daten von der Website
    # data = ... (hier Web Scraping-Code einfügen)
    return data

# Funktion zum Abrufen von Daten über eine API
def get_data_from_api(api_key):
    # Rufen Sie die API auf und erhalten Sie die Daten
    # data = ... (hier API-Aufruf-Code einfügen)
    return data

# Einlesen der Daten aus der CSV-Datei
csv_file = "football_data.csv"
data_csv = read_data_from_csv(csv_file)

# Web Scraping von Daten von einer Website
website_url = "https://example.com/football_data"
data_website = scrape_data_from_website(website_url)

# API-Zugriff für Wetterdaten
api_key = "your_api_key"
api_url = f"https://api.weather.com/football_data?api_key={api_key}"
data_api = get_data_from_api(api_url)

# Kombinieren der Daten aus verschiedenen Quellen (z. B. CSV, Website, API)
# data_combined = ... (hier Code zum Zusammenführen der Daten einfügen)

# Trainingsdaten
X = data_combined[['GermanyGoals', 'Weather', 'HomeAdvantage']]
y = data_combined['FranceGoals']

# Aufteilen der Daten in Trainings- und Testdaten
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Lineares Regressionsmodell
linear_model = LinearRegression()
linear_model.fit(X_train, y_train)
linear_predictions = linear_model.predict(X_test)
linear_mse = mean_squared_error(y_test, linear_predictions)

# Decision Tree Regressor
tree_model = DecisionTreeRegressor()
tree_model.fit(X_train, y_train)
tree_predictions = tree_model.predict(X_test)
tree_mse = mean_squared_error(y_test, tree_predictions)

# Random Forest Regressor
forest_model = RandomForestRegressor()
forest_model.fit(X_train, y_train)
forest_predictions = forest_model.predict(X_test)
forest_mse = mean_squared_error(y_test, forest_predictions)

# Auswahl des besten Modells basierend auf dem MSE
best_model = min(linear_mse, tree_mse, forest_mse)

# Vorhersage für ein neues Spiel
new_match_weather = np.random.choice([0, 1])
new_match_home_advantage = np.random.choice([0, 1])
new_match = pd.DataFrame({'GermanyGoals': [2], 'Weather': [new_match_weather],
                          'HomeAdvantage': [new_match_home_advantage]})

# Vorhersage mit dem besten Modell
predicted_france_goals_best = None
if best_model == linear_mse:
    predicted_france_goals_best = linear_model.predict(new_match)
    model_name = "Lineares Regressionsmodell"
elif best_model == tree_mse:
    predicted_france_goals_best = tree_model.predict(new_match)
    model_name = "Decision Tree Regressor"
else:
    predicted_france_goals_best = forest_model.predict(new_match)
    model_name = "Random Forest Regressor"

print(f"{model_name}: Vorhersage für Deutschland {new_match['GermanyGoals'][0]}, "
      f"Wetter: {new_match_weather}, Heimvorteil: {new_match_home_advantage} - "
      f"{predicted_france_goals_best[0]:.2f} Tore für Frankreich")

# Ausgabe der Mean Squared Errors (MSE) für die Modelle
print(f"Lineares Regressionsmodell MSE: {linear_mse:.2f}")
print(f"Decision Tree Regressor MSE: {tree_mse:.2f}")
print(f"Random Forest Regressor MSE: {forest_mse:.2f}")
